import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2){
            System.out.println("Usage: programm port ip");
            return;
        }
        try {
            Client client = new Client(Integer.parseInt(args[0]), args[1]);
            client.run();
        } catch (IOException exc) {
            System.err.println(exc.getMessage());
        }
    }
}
