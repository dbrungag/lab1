import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.time.Instant;
import java.util.HashMap;

public class Receiver implements Runnable {
    private MulticastSocket socket;
    private HashMap<String, Long> copies;
    private int timeout = 10;

    public Receiver(MulticastSocket ms) {
        socket = ms;
        copies = new HashMap<>();
    }

    public void run() {
        byte[]buf = new byte[256];
        while (true) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            long tmp;
            boolean inserted = false;
            int cond;
            String msg;
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            msg = new String(packet.getData(), 0, packet.getLength());
            tmp = Instant.now().getEpochSecond();
            if (copies.put(msg, tmp) == null) {
                inserted = true;
            }
            cond = copies.hashCode();
            copies.entrySet().removeIf(entry -> tmp - entry.getValue() > timeout);
            if (inserted || cond != copies.hashCode()) {
                System.out.println(copies.keySet());
            }
        }
    }
}
