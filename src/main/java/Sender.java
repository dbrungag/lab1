import java.io.IOException;
import java.net.*;

public class Sender implements Runnable {
    private InetAddress group;
    private MulticastSocket socket;
    private Integer portNmb;
    private int sendFreq = 1000;

    public Sender(InetAddress gr, MulticastSocket ms, Integer nmb) {
        group = gr;
        socket = ms;
        portNmb = nmb;
    }

    public void run() {
        String msg;
        byte[]buf;
        DatagramPacket packet;
        try {
            msg = InetAddress.getLocalHost().getHostAddress();
            buf = msg.getBytes();
            packet = new DatagramPacket(buf, buf.length, group, portNmb);
            while (true) {
                socket.send(packet);
                Thread.sleep(sendFreq);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }
}
