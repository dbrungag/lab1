import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Client {
    private MulticastSocket socket;
    private InetAddress group;
    private Integer portNmb;

    public Client(Integer nmb, String address) throws IOException {
        group = InetAddress.getByName(address);
        portNmb = nmb;
        socket = new MulticastSocket(nmb);
        socket.joinGroup(group);
    }

    public void run() {
        Sender sender = new Sender(group, socket, portNmb);
        Receiver receiver = new Receiver(socket);
        Thread thread1 = new Thread(sender);
        Thread thread2 = new Thread(receiver);
        thread1.start();
        thread2.start();
    }
}
